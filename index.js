
db.hotel.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
})


db.hotel.insertMany(
	[
		{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
		},
		{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
		}
	]
)

db.hotel.find({name: "double"})


db.hotel.updateOne(
	{ name: "queen" },
	{
		$set: {
			name: "queen",
			roomsAvailable: 0,

		}
	}
)

db.hotel.deleteMany({
	roomsAvailable: 0,
})

db.hotel.find()

